/***
 * @Author: zsh
 * @Date: 2020-11-19 14:31:58
 * @LastEditors: zsh
 * @LastEditTime: 2021-01-31 00:16:43
 * @FilePath: /homecredit/src/main.js
 **/
import Vue from 'vue'
import 'lib-flexible'
import App from './App.vue'
import router from '@/router'
import store from '@/store'
import 'utils/permission'
import SvgIcon from 'components/SvgIcon'
import PageLoading from 'components/Loading'
import '@/icons' // icon
import '@/style/common.scss'
import {
  Lazyload,
  Swipe,
  SwipeItem,
  Tab,
  Tabs,
  Loading,
  Overlay,
  Toast,
  List,
  Popover
} from 'vant'
import waterfall from 'vue-waterfall2'
import defaultSettings from '@/settings'

import Bridge from 'utils/JSbridge.js' // 判断机型 android or ios
import { onPageLinkPress } from 'utils/page-router.js' // 跳转方法
import Exposure from 'utils/exposure.js' // 曝光流

// options 为可选参数，无则不传
Vue.use(Lazyload)
Vue.use(Swipe)
Vue.use(SwipeItem)
Vue.use(Tab)
Vue.use(Tabs)
Vue.use(Loading)
Vue.use(Overlay)
Vue.use(Toast)
Vue.use(List)
Vue.use(Popover)
Vue.use(waterfall)

Vue.component('svg-icon', SvgIcon)
Vue.component('page-loading', PageLoading)

// 移动端调试工具
// if (process.env.ENV_INFO !== 'pro' && defaultSettings.vconsole) {
//   const VConsole = require('vconsole')
//   // eslint-disable-next-line
//   const my_console = new VConsole()
// }

const exp = new Exposure()
Vue.directive('exp-dot', {
  bind(el, binding, vnode) {
    exp.add({ el: el, val: binding.value })
  }
})

Vue.prototype.$bridge = Bridge
Vue.prototype.$onPageLinkPress = onPageLinkPress
Vue.prototype.$bus = new Vue()

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
