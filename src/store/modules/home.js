/***
 * @Author: zsh
 * @Date: 2020-12-07 23:57:50
 * @LastEditors: zsh
 * @LastEditTime: 2020-12-20 15:46:57
 * @FilePath: /homecredit/src/store/modules/home.js
 **/
// import { fetchLoad } from 'api/home'
// import { Toast } from 'vant'

export default {
  namespaced: true,
  state: {
    trackInfo: {}
  },
  mutations: {
    setTrackInfo(state, data) {
      state.trackInfo = data
    }
  },
  actions: {},
  getters: {}
}
