/***
 * @Author: zsh
 * @Date: 2020-12-14 22:36:42
 * @LastEditors: zsh
 * @LastEditTime: 2021-01-28 23:24:33
 * @FilePath: /homecredit/src/utils/track-event.js
 **/
import Bridge from './JSbridge.js'

export default {
  urlRoute(name, params = {}) {
    let data = {
      mixpanelEventName: name,
      eventType: 'mixPanel',
      eventData: {
        url: window.location.href,
        ...params
      }
    }
    Bridge.callHandler('trackEvent', data, res => {
      // console.log('调用urlRoute')
    })
  },

  click(name, params = {}) {
    let data = {
      mixpanelEventName: name,
      eventType: 'mixPanel',
      eventData: {
        ...params
      }
    }
    // console.log('即将调用trackEvent-->click', name) // zsh-log

    Bridge.callHandler('trackEvent', data, res => {
      // console.log('调用click')
    })
  },

  dwh(name, params = {}) {
    let data = {
      backendEventName: name,
      eventType: 'backEnd',
      eventData: {
        ...params
      }
    }

    Bridge.callHandler('trackEvent', data, res => {
      // console.log('调用click')
    })
  },

  // 搜索商品路由记录
  searchRoute(name, params = {}) {
    const session = sessionStorage.getItem('rpos_search_session_id')
    if (session !== undefined && session !== null && session !== '') {
      let data = {
        mixpanelEventName: name,
        eventType: 'mixPanel',
        eventData: {
          ...params,
          rpos_search_session_id: session
        }
      }
      Bridge.callHandler('trackEvent', data, res => {
        // console.log('调用searchRoute')
      })
    }
  }
}
