/***
 * @Author: zsh
 * @Date: 2020-11-19 14:29:22
 * @LastEditors: zsh
 * @LastEditTime: 2020-12-07 19:18:23
 * @FilePath: /homecredit/src/utils/index.js
 **/
/**
 * @param {string} url
 * @returns {Object}
 */
export function param2Obj(url) {
  const search = url.split('?')[1]
  if (!search) {
    return {}
  }
  return JSON.parse(
    '{"' +
      decodeURIComponent(search)
        .replace(/"/g, '\\"')
        .replace(/&/g, '","')
        .replace(/=/g, '":"')
        .replace(/\+/g, ' ') +
      '"}'
  )
}

export function deepClone(source) {
  if (!source && typeof source !== 'object') {
    throw new Error('error arguments', 'deepClone')
  }
  const targetObj = source.constructor === Array ? [] : {}
  Object.keys(source).forEach(keys => {
    if (source[keys] && typeof source[keys] === 'object') {
      targetObj[keys] = deepClone(source[keys])
    } else {
      targetObj[keys] = source[keys]
    }
  })
  return targetObj
}

/**
 * 防抖
 * @param {*} fn 将执行的函数
 * @param {*} delay 指定防抖持续时间
 */
export function debounce(fn, delay) {
  let timer = null

  return function() {
    if (timer) {
      clearTimeout(timer)
    }
    timer = setTimeout(() => {
      fn.apply(this, arguments)
    }, delay)
  }
}
/**
 * 节流
 * @param {*} fn 将执行的函数
 * @param {*} delay 节流规定的时间
 */
export function throttle(fn, delay) {
  let timer = null

  return function() {
    // 若timer === false，则执行，并在指定时间后将timer重制
    if (!timer) {
      fn.apply(this, arguments)

      timer = setTimeout(() => {
        timer = null
      }, delay)
    }
  }
}
