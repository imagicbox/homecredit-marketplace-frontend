/***
 * @Author: zsh
 * @Date: 2020-11-19 14:22:17
 * @LastEditors: zsh
 * @LastEditTime: 2021-01-28 23:26:21
 * @FilePath: /homecredit/src/utils/request.js
 **/
import axios from 'axios'
import { Toast } from 'vant'
import store from '@/store'
import { getToken } from '@/utils/auth'
import { envPublicKey, envAuthorization } from './env-config'

// create an axios instance
const service = axios.create({
  // baseURL:
  //   process.env.NODE_ENV === 'development'
  //     ? process.env.VUE_APP_BASE_API
  //     : envRequestUrl(), // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  timeout: 30000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent
    // if (store.getters.token) {
    //   config.headers['Authorization'] = `Bearer ${getToken()}`
    // }
    // console.log('config===>>>', config.data) // zsh-log

    config.headers['Public-Key'] = envPublicKey()
    config.headers['Authorization'] = envAuthorization()

    return config
  },
  error => {
    // do something with request error
    // console.log(error, 'err') // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data

    // if the custom code is not 20000, it is judged as an error.
    if (res.errcode !== 0) {
      // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
      // if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
      //   // to re-login
      //   Toast.confirm(
      //     'You have been logged out, you can cancel to stay on this page, or log in again',
      //     'Confirm logout',
      //     {
      //       confirmButtonText: 'Re-Login',
      //       cancelButtonText: 'Cancel',
      //       type: 'warning'
      //     }
      //   ).then(() => {
      //     store.dispatch('user/resetToken').then(() => {
      //       location.reload()
      //     })
      //   })
      // }
      return Promise.reject(new Error(res.errmsg || 'Error'))
    } else {
      return res
    }
  },
  error => {
    // console.log('err:', error) // for debug
    // Toast.fail({
    //   message: error.message,
    //   duration: 1.5 * 1000
    // })
    return Promise.reject(error)
  }
)

export default service
