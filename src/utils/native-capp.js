/***
 * @Author: zsh
 * @Date: 2020-12-13 22:26:31
 * @LastEditors: zsh
 * @LastEditTime: 2021-01-28 23:25:19
 * @FilePath: /homecredit/src/utils/native-capp.js
 **/
import Bridge from './JSbridge.js'

// 验证用户状态customer,user,visitor
export function getLoginStatus() {
  let data = 'visitor'

  const res = Bridge.call('getLoginStatus', '{}')
  // console.log('获取app响应数据:' + res)
  if (res) {
    data = JSON.parse(res).status
  }

  return data
}

// 获取cuid及fackid
export function getCommonData() {
  const status = getLoginStatus()
  let data = ''

  if (status === 'customer' || status === 'user') {
    const res = Bridge.call('getIdentifier', '{}')
    // console.log('获取app响应数据:' + res)
    if (res) {
      data = {
        ...JSON.parse(res),
        status
      }
    }
  }

  return data
}

// 登录(可以带request参数)
export function toLogin(params) {
  Bridge.callHandler('toLogin', params, res => {
    // console.log('去登录:' + res)
  })
}

// 登录成功
export function goAfterLogin() {
  Bridge.registerHandler('goAfterLogin', (data, responseCallback) => {
    // alert('app主动调用js方法:' + data)
    const res = JSON.parse(data)
    // responseCallback(data)
    switch (res.operationType) {
      case 'common':
      case 'culate':
        // getCommonData();
        break
      case 'createOrder':
        // createOrder();
        break
      case 'default':
        window.location.reload()
        break
    }
  })
}

// 捷信惠购
export function toHcPay() {
  Bridge.callHandler('toHcPay', '{}', res => {
    // console.log('去捷信惠购:' + res)
  })
}

// 积分会员中心
export function toLoyaltySystem(params) {
  Bridge.callHandler('toLoyaltySystem', params, res => {
    // console.log('toLoyaltySystem:' + res)
  })
}

export function h5PageLoaded() {
  Bridge.callHandler('h5PageLoaded', '{}', res => {
    // console.log('h5PageLoaded:' + res)
  })
}

// ORBP
export function startToORBP(params) {
  Bridge.callHandler('startToORBP', params, res => {
    // console.log('startToORBP:' + res)
  })
}

// ORBP返回刷新
export function goAfterPreAf() {
  // console.log('goAfterPreAf调用,即将调用Bridge===>>>') // zsh-log

  Bridge.registerHandler('goAfterPreAf', (data, responseCallback) => {
    window.location.reload()
  })
}

// 刷新token
export function refreshToken() {
  return new Promise((resolve, reject) => {
    Bridge.callHandler('refreshToken', '{}', res => {
      // console.log('refreshToken:' + res)
      const response = JSON.parse(res)
      if (response.code === 0) {
        resolve(response)
      } else {
        notifyTokenExpired()
      }
    })
  })
}

// H5无法获取合法token时通知Native(不展示错误页面跳转至原生处理)
export function notifyTokenExpired() {
  Bridge.callHandler('notifyTokenExpired', '{}', res => {
    // console.log('notifyTokenExpired:' + res)
  })
}

// 内部 or 外部 打开链接
export function toWebPageWithUrl(params) {
  Bridge.callHandler('toWebPageWithUrl', params, res => {
    // console.log('toWebPageWithUrl:' + res)
  })
}
