import defaultSettings from '@/settings'

const title = defaultSettings.title || 'capp'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
