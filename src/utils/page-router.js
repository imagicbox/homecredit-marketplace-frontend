/***
 * @Author: zsh
 * @Date: 2020-12-07 00:01:41
 * @LastEditors: zsh
 * @LastEditTime: 2021-03-01 15:29:46
 * @FilePath: /homecredit/src/utils/page-router.js
 **/

import router from '@/router'
import { Toast } from 'vant'
import { envRouterUrl } from './env-config'
import {
  getLoginStatus,
  toLogin,
  startToORBP,
  toHcPay,
  toWebPageWithUrl
} from './native-capp'

// 挂件路由跳转
export function onPageLinkPress(route) {
  // console.log('1===>>>', route, envRouterUrl()) // zsh-log

  if (!route.linktype || route.linktype === '') return

  let params = null
  const url = envRouterUrl()

  const status = getLoginStatus()

  // capp页面
  switch (route.linktype) {
    case 'member':
      // 个人中心
      switch (status) {
        case 'customer':
        case 'user':
          window.location.href = `${url}/member.html`
          break
        case 'visitor':
          toLogin({
            operationType: 'default'
          })
          break
      }
      break

    case 'favorite':
      // 关注
      switch (status) {
        case 'customer':
        case 'user':
          window.location.href = `${url}/productCollect.html`
          break
        case 'visitor':
          toLogin({
            operationType: 'default'
          })
          break
      }
      break

    case 'category':
      // 类目页
      window.location.href = `${url}/category.html`
      break

    case 'search':
      // 搜索页
      window.location.href = `${url}/search.html`
      break

    case 'item':
      // 商品详情页
      localStorage.setItem('product_id', route.linktarget)
      window.location.href = `${url}/productInfo.html?item_id=${route.linktarget}`
      break

    case 'catlist':
      // 三级分类页
      localStorage.removeItem('brand_id')
      localStorage.setItem('cat_id', route.linktarget)
      window.location.href = `${url}/gallery.html`
      break

    case 'shop':
      // 店铺页
      localStorage.setItem('shop_id', route.linktarget)
      window.location.href = `${url}/storeHome.html`
      break

    case 'toolactivity':
      // 专题页
      localStorage.setItem('activity_id', route.linktarget)
      window.location.href = `${url}/activity.html`
      break

    case 'toolintroduce':
      // 介绍页
      localStorage.setItem('introduce_id', route.linktarget)
      window.location.href = `${url}/introduce.html`
      break

    case 'flashsale':
      // 秒杀页
      window.location.href = `${url}/timesale.html`
      break

    case 'voucher':
      // 购物券页
      switch (status) {
        case 'customer':
        case 'user':
          window.location.href = `${url}/voucher.html`
          break
        case 'visitor':
          toLogin({
            operationType: 'default'
          })
          break
      }
      break

    case 'trade':
      // 我的订单页
      switch (status) {
        case 'customer':
        case 'user':
          window.location.href = `${url}/tradeList.html`
          break
        case 'visitor':
          toLogin({
            operationType: 'default'
          })
          break
      }
      break

    case 'hcpay':
      // 捷信惠购
      switch (status) {
        case 'customer':
          toHcPay()
          break
        case 'user':
          Toast('此功能暂未对您开放')
          break
        case 'visitor':
          toLogin({
            operationType: 'default'
          })
          break
      }
      break

    case 'popactivity':
      // POP专题页
      localStorage.setItem('popactivity_id', route.linktarget)
      window.location.href = `${url}/popActivity.html`
      break

    case 'sudoku':
      // 九宫格抽奖
      switch (status) {
        case 'customer':
        case 'user':
          localStorage.setItem('sudoku_id', route.linktarget)
          window.location.href = `${url}/sudoku.html`
          break
        case 'visitor':
          toLogin({
            operationType: 'default'
          })
          break
      }
      break

    case 'prepaid':
      // 手机充值
      switch (status) {
        case 'customer':
        case 'user':
        case 'visitor':
          localStorage.setItem('prepaid', route.linktarget)
          window.location.href = `${url}/prepaid.html`
          break
      }

      break

    case 'on-site':
      // 站内打开
      toWebPageWithUrl({
        isInternal: true,
        pageUrl: route.linktarget,
        title: {
          titleStyle: 'DYNAMIC',
          content: '活动页'
        }
      })
      break

    case 'out-site':
      // 站外打开
      toWebPageWithUrl({
        isInternal: false,
        pageUrl: route.linktarget
      })
      break

    case 'native_orbp':
      switch (status) {
        case 'customer':
          // 去测评
          let campaignSubType = ''
          if (localStorage.getItem('userInfo')) {
            const userInfo = JSON.parse(localStorage.getItem('userInfo'))
            campaignSubType = userInfo.campaignSubType
            if (userInfo.status === 'P8E8') {
              return
            }
          }

          if (status === 'customer') {
            startToORBP({
              orbpStyle: 'returned',
              requestName: 'banner',
              campaignSubtype: campaignSubType
            })
          }
          break
        case 'user':
          Toast('此功能暂未对您开放')
          break
        case 'visitor':
          toLogin({
            operationType: 'default'
          })
          break
      }
      break

    case 'tmpl_self':
      params = {
        path: '/home/' + route.linktarget
      }
      break

    case 'business':
      // 经营许可证
      window.location.href = `${url}/business.html`
      break

    default:
      params = {
        path: '/home/' + new Date().getTime()
      }
      break
  }

  router.push(params)
}
