/***
 * @Author: zsh
 * @Date: 2020-11-19 18:48:25
 * @LastEditors: zsh
 * @LastEditTime: 2021-01-28 23:26:59
 * @FilePath: /homecredit/src/utils/JSbridge.js
 **/

const isAndroid =
  navigator.userAgent.indexOf('Android') > -1 ||
  navigator.userAgent.indexOf('Adr') > -1
const isiOS = !!navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)

/***
 *
 * 目前安卓使用dsBridge
 *
 * ios使用jsBridge
 *
 **/
const dsBridge = require('dsbridge')
// 这是必须要写的，用来创建一些设置
function setupWebViewJavascriptBridge(callback) {
  // iOS使用
  // if (isiOS) {
  if (window.WebViewJavascriptBridge) {
    return callback(window.WebViewJavascriptBridge)
  }
  if (window.WVJBCallbacks) {
    return window.WVJBCallbacks.push(callback)
  }
  window.WVJBCallbacks = [callback]
  var WVJBIframe = document.createElement('iframe')
  WVJBIframe.style.display = 'none'
  WVJBIframe.src = 'wvjbscheme://__BRIDGE_LOADED__'
  document.documentElement.appendChild(WVJBIframe)
  setTimeout(() => {
    document.documentElement.removeChild(WVJBIframe)
  }, 0)
  // sessionStorage.phoneType = 'ios'
  // }
}

export default {
  // js调APP方法 （参数分别为:app提供的方法名  传给app的数据  回调）
  call(name, data) {
    // console.log('js调APP方法=同步', name, data) // zsh-log
    if (isAndroid) {
      // console.log('tag', '安卓')
      return dsBridge.call(name, data)
    }
    if (isiOS) {
      // console.log('tag', 'ios')
      return setupWebViewJavascriptBridge(bridge => {
        return bridge.call(name, data)
      })
    }
  },
  callHandler(name, data, callback) {
    // console.log('js调APP方法=异步', name, data, callback) // zsh-log
    if (isAndroid) {
      // console.log('tag', '安卓')
      dsBridge.call(name, data, callback)
    }
    if (isiOS) {
      // console.log('tag', 'ios')
      setupWebViewJavascriptBridge(bridge => {
        bridge.callHandler(name, data, callback)
      })
    }
  },
  // APP调js方法 （参数分别为:js提供的方法名  回调）
  registerHandler(name, callback) {
    // console.log('APP调js方法', name, callback) // zsh-log
    if (isAndroid) {
      // console.log('tag', '安卓')
      dsBridge.registerAsyn(name, (data, responseCallback) => {
        callback(data, responseCallback)
      })
    }
    if (isiOS) {
      // console.log('tag', 'ios')
      setupWebViewJavascriptBridge(bridge => {
        bridge.registerHandler(name, (data, responseCallback) => {
          callback(data, responseCallback)
        })
      })
    }
  }
}

// 使用演示
// js 调用 app
// this.$bridge.call('dataToAndroid',msg)

// this.$bridge.callHandler('dataToAndroid',msg,(res)=>{
//   alert('获取app响应数据:' + res)
// })

// app 调用 js
// this.$bridge.registerHandler('dataToJs', (data, responseCallback) => {
//   alert('app主动调用js方法，传入数据:'+ data)
//   responseCallback(data)
// })
