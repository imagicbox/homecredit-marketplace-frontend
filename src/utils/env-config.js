/***
 * @Author: zsh
 * @Date: 2020-12-12 21:12:10
 * @LastEditors: zsh
 * @LastEditTime: 2020-12-14 21:42:15
 * @FilePath: /homecredit/src/utils/env-config.js
 **/

const { ENV_INFO } = process.env

// 静态资源 CDN 地址前缀: https://imageshopex.homecreditcfc.cn

const base = {
  pro: {
    public_key: 'zrKD/WDiTV5jg9pBiIqHeGNSWJwn/7wp9A7Qv9nGIaYGH3OPJ6OcYNg1crPOBB1oh3mKp64ZSL1LQyGxNWo8Ng==',
    authorization: 'Basic Vm9DcXp5b3U6SzNvQzZKa00=',
    request_url: 'https://www.weiqionlineshop.com/index.php/wap',
    old_request_url: 'https://www.weiqionlineshop.com/index.php/capi',
    middleware_url: 'https://www.weiqionlineshop.com/middleware',
    route_url: 'https://www.weiqionlineshop.com/capp'
  },
  uat: {
    public_key: 'zrKD/WDiTV5jg9pBiIqHeGNSWJwn/7wp9A7Qv9nGIaYGH3OPJ6OcYNg1crPOBB1oh3mKp64ZSL1LQyGxNWo8Ng==',
    authorization: 'Basic Vm9DcXp5b3U6SzNvQzZKa00=',
    request_url: 'https://shopex.homecreditcfc.cn/index.php/wap',
    old_request_url: 'https://shopex.homecreditcfc.cn/index.php/capi',
    middleware_url: 'https://shopex.homecreditcfc.cn/middleware',
    route_url: ' https://shopex.homecreditcfc.cn/capp'
  },
  sit: {
    public_key: 'c9weShhcsodpBAF6T+CzK/P8bziPvRBjg/7IS95lMIiIlDDYJktDyXtvs5v7BP3LbgtcWGBvJEkZFD9k76LvFg==',
    authorization: 'Basic RVlDbVo3dUY6QzB1TG55Rmw=',
    request_url: 'https://shopexsit.homecreditcfc.cn/index.php/wap',
    old_request_url: 'https://shopexsit.homecreditcfc.cn/index.php/capi',
    middleware_url: 'https://shopexsit.homecreditcfc.cn/middleware',
    route_url: ' https://shopexsit.homecreditcfc.cn/capp'
  },
  pre: {
    public_key: 'zrKD/WDiTV5jg9pBiIqHeGNSWJwn/7wp9A7Qv9nGIaYGH3OPJ6OcYNg1crPOBB1oh3mKp64ZSL1LQyGxNWo8Ng\u003d\u003d',
    authorization: 'Basic Vm9DcXp5b3U6SzNvQzZKa00=',
    request_url: 'https://shopexuat.homecreditcfc.cn/index.php/wap',
    old_request_url: 'https://shopexuat.homecreditcfc.cn/index.php/capi',
    middleware_url: 'https://shopexuat.homecreditcfc.cn/middleware',
    route_url: 'https://shopexuat.homecreditcfc.cn/capp'
  },
  dev: {
    public_key: 'zrKD/WDiTV5jg9pBiIqHeGNSWJwn/7wp9A7Qv9nGIaYGH3OPJ6OcYNg1crPOBB1oh3mKp64ZSL1LQyGxNWo8Ng==',
    authorization: 'Basic Vm9DcXp5b3U6SzNvQzZKa00=',
    request_url: 'http://jiexin.imagicbox.cn/index.php/wap',
    old_request_url: 'http://jiexin.imagicbox.cn/index.php/capi',
    middleware_url: 'http://middleware.imagicbox.cn',
    route_url: 'http://jiexin.imagicbox.cn/capp'
  }
}

export function envPublicKey() {
  return base[ENV_INFO].public_key
}
export function envAuthorization() {
  return base[ENV_INFO].authorization
}
export function envRequestUrl() {
  return base[ENV_INFO].request_url
}
export function envOldRequestUrl() {
  return base[ENV_INFO].old_request_url
}
export function envMiddlewareUrl() {
  return base[ENV_INFO].middleware_url
}
export function envRouterUrl() {
  return base[ENV_INFO].route_url
}
