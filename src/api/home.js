/***
 * @Author: zsh
 * @Date: 2020-12-06 20:55:08
 * @LastEditors: zsh
 * @LastEditTime: 2021-01-30 23:28:14
 * @FilePath: /homecredit/src/api/home.js
 **/
import request from '@/utils/request'
import {
  envRequestUrl,
  envOldRequestUrl,
  envPublicKey
} from '@/utils/env-config'
import { getCommonData } from '@/utils/native-capp'

// 挂件加载
export function fetchLoad(params) {
  return request({
    url: params.tmpl_id ? envRequestUrl() + '/customize' : envRequestUrl(),
    method: 'post',
    data: {
      module: 'widget',
      method: 'index',
      params: {
        user_name: getCommonData(),
        user_info: localStorage.getItem('userInfo')
          ? JSON.parse(localStorage.getItem('userInfo'))
          : '',
        ...params
      }
    }
  })
}

// offer 信息
export function offerInfoRequest() {
  return request({
    url: envRequestUrl() + '/lite_user_info',
    method: 'post',
    data: {
      params: {
        user_name: getCommonData()
      }
    }
  })
}

// 横向橱窗
export function goodsHorizontal({ method, module, params }) {
  return request({
    url: envOldRequestUrl(),
    method: 'post',
    data: {
      method,
      module,
      params: {
        ...params,
        user_name: getCommonData()
      },
      public_key: envPublicKey()
    }
  })
}
// 纵向橱窗
export function goodsListLoad({ method, module, params }) {
  return request({
    url: envOldRequestUrl(),
    method: 'post',
    data: {
      method,
      module,
      params: {
        ...params,
        user_name: getCommonData(),
        price_by_be: 1
      },
      public_key: envPublicKey()
    }
  })
}
// 推广
export function promotionChannel(params) {
  return request({
    url: envOldRequestUrl(),
    method: 'post',
    data: {
      method: 'channelActivity',
      module: 'activity',
      params: {
        ...params,
        user_name: getCommonData()
      },
      public_key: envPublicKey()
    }
  })
}

// 'public_key': capp.public_key,
// 'module': 'widget',
// 'method': 'index',
// 'params': {
//     'user_status': user_status, // 三期pop
//     'user_limit': user_limit, // 三期pop
//     'user_name': nCapp.getCommonData() ? nCapp.getCommonData() : ""
// }
