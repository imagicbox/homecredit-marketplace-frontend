/***
 * @Author: zsh
 * @Date: 2020-12-15 01:38:32
 * @LastEditors: zsh
 * @LastEditTime: 2021-01-28 23:28:49
 * @FilePath: /homecredit/src/api/getUserInfo.js
 **/
import request from '@/utils/request'
import { envOldRequestUrl, envPublicKey } from '@/utils/env-config'
import {
  getCommonData,
  refreshToken,
  notifyTokenExpired
} from '@/utils/native-capp'

let critical = 0

export function getUserInfo() {
  return new Promise((resolve, reject) => {
    request({
      url: envOldRequestUrl(),
      method: 'post',
      data: {
        public_key: envPublicKey(),
        module: 'member',
        method: 'userInfo',
        params: {
          user_name: getCommonData()
        }
      }
    })
      .then(res => {
        // console.log('getUserInfo:', res) // zsh-log
        if (res.errcode !== 0) {
          if (res.errcode === 10039) {
            if (critical < 5) {
              critical += 1
              refreshToken().then(() => {
                getUserInfo()
              })
            } else {
              critical = 0
              notifyTokenExpired()
            }
          }
        } else {
          critical = 0
          const userInfo = res.data.userInfo
            ? JSON.stringify(res.data.userInfo)
            : ''
          localStorage.setItem('userInfo', userInfo)

          resolve(res)
        }
      })
      .catch(err => {
        // console.log('getUserInfo err', err) // zsh-log
      })
  })
}
