/***
 * @Author: zsh
 * @Date: 2020-12-14 21:53:16
 * @LastEditors: zsh
 * @LastEditTime: 2021-01-28 23:28:40
 * @FilePath: /homecredit/src/api/middleware.js
 **/
import axios from 'axios'
// import router from '@/router'
import trackEvent from '@/utils/track-event'
import { getCommonData } from '@/utils/native-capp'
import { envAuthorization, envMiddlewareUrl } from '@/utils/env-config'

let behaviorBeginTime = ''
let behaviorEndTime = ''

const middlewareReq = axios.create({
  headers: {
    'Content-Type': 'application/json;charset=utf-8',
    Authorization: envAuthorization()
  },
  timeout: 30000 // request timeout
})

// request interceptor
middlewareReq.interceptors.request.use(config => {
  behaviorBeginTime = Date.now()
  return config
})

export default {
  sendBehavior(data) {
    let routeName = ''
    if (data.route_name) {
      routeName = data.route_name
      delete data.route_name
    }

    if (!data.cuid) {
      data.cuid = getCommonData()
    }
    if (!data.fakeId) {
      data.fakeid = getCommonData().userId || ''
    }
    // console.log('Behavior post:', data)
    middlewareReq({
      url: envMiddlewareUrl() + '/behavior',
      method: 'post',
      data
    })
      .then(() => {
        // const url = window.location.href

        // if (url.indexOf('index') > -1) {
        if (routeName === 'home') {
          behaviorEndTime = Date.now()

          trackEvent.click('RPOS_HP_SHOPEX_CALL_BIGDATA_INTERFACE', {
            RPOS_START_TIME: behaviorBeginTime,
            RPOS_FINISH_TIME: behaviorEndTime
          })
        }
      })
      .catch(err => {
        console.log('middleware err:', err) // zsh-log
      })
  }
}
// {"scene":"detail","action":"A3","window_id":"1","item_id":"3213","cat_id":"123","title":"测试商品","show_type":false,"model_tag":false,"sort_num":false}
